import express from 'express';
import bodyParser from 'body-parser';

// Set up the express server
const server = express();

// Load Bunyan logger
let bunyan = require('bunyan');

let cors = require('cors');

// SDK logger
let sdkLogger = bunyan.createLogger({
  name: 'sdk',
  stream: process.stdout,
  level: 'info'
});

// Application logger
let logger = bunyan.createLogger({
  name: 'server',
  stream: process.stdout,
  level: 'info'
});

// RxJS / Observables
let Rx = require('rx');

// Circuit SDK
let Circuit = require('circuit-sdk');

var request = require('request');

// Setup bunyan logger
Circuit.setLogger(sdkLogger);

// Application variables
let client;

let conversations = new Map();

server.use(cors())

// Parse incoming requests data
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));

const PORT = normalizePort(process.env.PORT || '7500');
server.set('port', PORT);

// info
server.get('/api/v1/info', (req, res) => {
  res.status(200).send({
    message: `server running on port ${PORT}`
  })
});

// create conversations
server.post('/api/v1/groupconversation', (req, res) => {
  let error = "";
  var roomTitle = req.query.roomtitle == undefined? "Hyperion xxx": req.query.roomtitle;
  var initialText = req.query.initialtext == undefined? "Neuer Raum eröffnet. Sie können jetzt ihre Konversation starten": req.query.initialtext;
  var participants = req.query.participants.split(",")
  client = getLogin()
  client.logon()
    .then(user => console.log('Successfully authenticated as ' + user.displayName))
    .then(() => client.getUsersByEmail(participants))
    .then(users => users.map(user => user.userId))
    .then(userIds => client.createGroupConversation(userIds, roomTitle))
    .then(conv => client.addTextItem(conv.convId, initialText))
    .then(client.logout)
    .catch((err) => {
      error = err
      logger.debug('Error: ' + err);
    });

  error = "none";
  setTimeout(function() {
    callbackResponse(res, error, participants);
  }, 2500);
});

server.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});

//*********************************************************************
// Helper functions
//*********************************************************************
function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }
  return false;
}

function callbackResponse(res, err) {
  if(err == "none"){
    return res.status(200).send({
      code: 200,
      message: 'conversation successfully created'
    });
  }

  handleErrorCode(res, err);
}

function handleErrorCode(res, err) {
  var code = err.code;

  switch(code) {
    case 'AUTHORIZATION_FAILED':
      return res.status(401).send({
        errorID: 'E0000004',
        statusCode: 401,
        timestamp: new Date(),
        message: 'conversation can not be created ' + err.code,
        messageDe: 'conversation kann nicht gestartet werden ' + err.code,
        messageEn: 'conversation can not be created ' + err.code,
        messageFr: 'la crèation de la conversation a èchouèe ' + err.code,
        messageIt: 'la conversazione non può essere creata ' + err.code,
        debugMessage: err.stack
      });
    case 'SDK_ERROR':
      return res.status(400).send({
        errorID: 'E0000027',
        statusCode: 400,
        timestamp: new Date(),
        message: 'conversation can not be created: one or more of your enduser are not a circuit user',
        messageDe: 'Konversation kann nicht erstellt werden. Einer oder mehrere Ihrer Endbenutzer sind keine Circuit Benutzer',
        messageEn: 'conversation can not be created: one or more of your enduser are not a circuit user',
        messageFr: 'la conversation ne peut pas être créée: un ou plusieurs de vos utilisateurs finaux ne sont pas des utilisateurs de circuit',
        messageIt: 'la conversazione non può essere creata: uno o più utenti finali non sono utenti del circuito',
        debugMessage: err.stack
      });
    default:
      return res.status(500).send({
        errorID: 'E0000009',
        statusCode: 500,
        timestamp: new Date(),
        message: 'conversation can not be created ' + err.code,
        messageDe: 'conversation kann nicht gestartet werden ' + err.code,
        messageEn: 'conversation can not be created ' + err.code,
        messageFr: 'la crèation de la conversation a èchouèe ' + err.code,
        messageIt: 'la conversazione non può essere creata ' + err.code,
        debugMessage: err.stack
      });
  }
}

//*********************************************************************
// logon
//*********************************************************************
function getLogin() {
  let client = new Circuit.Client({
    client_id: '763e7f23849d4e23ac99e958150203f5',
    client_secret: '9aac58516a5d434bbbaed9b9a86e8ae6',
    domain: 'eu.yourcircuit.com',
    scope:'READ_USER_PROFILE,WRITE_USER_PROFILE,READ_CONVERSATIONS,WRITE_CONVERSATIONS,READ_USER,CALL_RECORDING,CALLS,MENTION_EVENT,USER_MANAGEMENT'
  });
  return client;
}

//*********************************************************************
// addEventListeners
//*********************************************************************
function addEventListeners(client) {
  // Just for the heck of it play with observables
  Rx.Observable.fromEvent(client, 'itemAdded')
    .map(evt => evt.item)
    .filter(item => { return conversations.get(item.convId) && item.type === 'TEXT'; })
    .subscribe(
      function (x) {
        logger.debug('itemAdded event. itemId=' + x.itemId);
      },
      function (err) {
        logger.debug('Error: ' + err);
      }
    );
}


